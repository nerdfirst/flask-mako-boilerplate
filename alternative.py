from flask import Flask
from flask_mako import MakoTemplates, render_template
from mako import exceptions

app = Flask(__name__)

# Hidden setting for better error messages
# Source: https://stackoverflow.com/a/21950573
app.config["MAKO_TRANSLATE_EXCEPTIONS"] = False

mako = MakoTemplates(app)

@app.route("/")
def page():
	return render_template("your_mako_template")

@app.errorhandler(500)
def server_error(e):
    # This function is run when an HTTP 500 error code is generated
    # The error template is rendered and the HTTP 200 code is emitted instead
    return exceptions.html_error_template().render()
	
if __name__ == "__main__":
	app.run()
	
	