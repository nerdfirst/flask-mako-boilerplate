# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Mako Templating Engine - Pinpoint #17**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/pJ_yheGsv6U/0.jpg)](https://www.youtube.com/watch?v=pJ_yheGsv6U "Click to Watch")

# Contents

This repository consists of two ways of displaying friendly error messages in a Flask-Mako project.

The first method, in `server.py`, is as described in the video linked above. You have the most control over it, but it's a hassle since you need a try-except block for each route.

If you have many routes, you can also consider `alternative.py`, which uses Flask's in-built method of handling HTTP error codes (in this case, I've set it to error 500) to trigger the same friendly error message.


# How to Use

This repository contains a boilerplate server page for use in Python/Flask/Mako setups.

First, install Mako by running the following command call on the command line:

`pip install Flask-Mako`

In a folder, place `server.py` or `alternative.py` from this repository into its root, and set up your html templates in a folder called `templates`. Implement routing and other logic into `server.py` or `alternative.py`, remembering to include the try-except block for pages that are work-in-progress so that errors are rendered onto the HTML output.

To test your program, simply run the `server.py`, for example via the command line with the following command call:

`python server.py` or `python alternative.py` (depending on which you are using)

**Security Warning:** When your pages are no longer in development, remove the error handling logic from each route so that error messages aren't shown to the users of your web application.

# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.