from flask import Flask
from flask_mako import MakoTemplates, render_template
from mako import exceptions

app = Flask(__name__)

# Hidden setting for better error messages
# Source: https://stackoverflow.com/a/21950573
app.config["MAKO_TRANSLATE_EXCEPTIONS"] = False

mako = MakoTemplates(app)

@app.route("/")
def page():
	try:
		return render_template("your_mako_template")
	except:
		# When any exception occurs, package it into a HTML template
		# Render it out so it can be seen on the page
		return exceptions.html_error_template().render()
	
if __name__ == "__main__":
	app.run()
	
	